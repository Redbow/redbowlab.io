+++
title = "About"
date = 2018-01-14T19:40:05+01:00
+++

Neue Webtechnologien und digitale Ausdrucksformen begeistern mich. Ich hoffe ich kann sie dann auch schnell einsetzen, zum Beispiel für Kundenprojekte. Ich konzentriere mich am liebsten auf die Frontend-Entwicklung, übernehme bei kleineren Projekten aber auch gern die Templateintegration. 

Brauchst du vielleicht eine **Website**? 
<br>
Oder eine Freelancerin, die sich auf die **Frontend-Entwicklung** stürzt?
<br>
Dann [schreib mir](#contact) einfach. 

Einen aktuellen Lebenslauf findest du auf <a target="_blank" href="https://xing.com/profile/Jana_Deppe2">Xing</a>.  

<!-- Ich bin auch Doktorandin der Medienwissenschaften, schreibe über “”. All diese Themen, Künstliche Intelligenz, Webtechnologien, meine Liebe zu Rollen-, Brett- und Computerspielen sowie alle interessanten Dinge, denen ich so begegne weben ich meinem Blog zusammen. Es ist ein aufregender Mix. Schau vorbei, wenn du Lust hast. Ich hoffe, auch du findest interessante, neue Dinge!  -->
