+++
title = "Newsletter"
description = "Newsletter abonnieren"
expiryDate = 2019-01-01
+++

<br>
**Hallo!** Wie einige von euch wissen, bestreite ich zur Zeit eine Fahrradtour von Athen nach Wien. Damit ich euch alle, die ihr Interesse daran habt, an meinem Abenteuer teilhaben lassen kann, habe ich einen E-Mail-Newsletter gestartet. Updates gibt es etwa einmal pro Woche (seid nicht zu streng mit mir), mit einem deutschen und einem englischen Text. Bitte tragt euch ein!

{{< figure src="/images/bike-tour-bikes.jpg" caption="Our touring bikes during our last tour through Scandinavia." >}}

**Hello!** As some of you may know, I am currently on a long bike trekking tour from Athens to Vienna. To let all of you, who are interested, know what I'm up to, I've created an email newsletter. It updates about once a week (but don't hold me to it too strictly, please). It's both in English and German. Please subscribe! 

{{< mailchimp >}}
