+++
title = "SinnerSchrader"
description = "Projektbericht zur Kooperation mit SinnerSchrader"
date = 2019-04-16T11:13:35+01:00
+++

Für einen Kunden der Digitalagentur SinnerSchrader in Hamburg habe ich eine **Single Page Application** weiterentwickelt. Die Umsetzung erfolgte mit **VueJS**. Ich habe die App um eine Vue Router und Vuex Integration erweitert, um die Maintainability zu erhöhen und leichter neue Features einbauen zu können. Ein großer Teil meiner Arbeit bestand aus dem Refactoring der bestehenden Code Base. 

Die App war weiterhin über eine **REST API** an eine Datenbank des Kunden angebunden um das Abrufen und Verarbeiten von Kundendaten zu ermöglichen. Darüber hinaus wurde auch die Landing Page des Kunden, basierend auf TYPO3 erweitert und überarbeitet. Auch hier galt es die zum Teil älteren, bestehenden Elemente zu modernisieren — sowohl visuell als auch technisch.

> Unsere Zusammenarbeit war sehr angenehm. Jana arbeitet lösungsorientiert und eigenverantwortlich. Die Qualität des Produkts steht dabei an erster Stelle. Wir freuen uns zukünftig auf eine gute und vertrauensvolle Zusammenarbeit.
<cite>Elena Kirstetter – Projektmanagerin, SinnerSchrader Deutschland GmbH</cite>

Die Teamarbeit lief fast ausschließlich remote ab und wurde koordiniert über **Slack und Jira**. Mir hat sowohl die Arbeit am Projekt, als auch die Zusammenarbeit mit dem Team sehr viel Freude bereitet. Durch fast tägliche, klare Kommunikation lief die Koordination der Aufgaben und Abnahme der Ergebnisse reibungslos ab. 

#### Verwendete Technologien

- [VueJS](https://vuejs.org)
- [Vue Router](https://router.vuejs.org)
- [Vuex](https://vuex.vuejs.org)
- [Webpack](https://webpack.js.org/)
- [Gitlab CI](https://about.gitlab.com/product/continuous-integration/)