+++
title = "Landmetzgerei Born"
description = "Projektbericht zur Landmetzgerei Born"
+++

Für die Landmetzgerei Born habe ich nach einem vorgegebenen Design sowohl Frontend- als auch Backend-Entwicklung übernommen. Dafür habe ich zum ersten Mal Grav CMS verwendet, ein flat-file-System (d.h. ohne Datenbank auskommend) mit Twig-Templates. Weiterhin habe ich das minimalistische Framework stimulus.js von den Basecamp-Entwicklern ausprobiert. Mit beiden Neulingen bin ich sehr zufrieden: schneller Einstieg, wenig Aufwand, viele Möglichkeiten! 

{{< figure src="/images/landmetzgereiborn-screenshot.png" >}}

Die einfach zu pflegenden, wöchentlich neuen Menüs für verschiedene Standorte sind ein zentrales Element der Seite. Solche individualisierten Inhaltstypen werden schnell in den blueprint-Datein von Grav definiert. Die Vorstellung der breiten Produktpalette ist ebenso ein Highlight, wie der Videohintergrund auf der Startseite. Mit Grav waren kleine Individualisierungen, wie die Gestaltung der Geschichts- oder der Kontaktseite leicht möglich. 

#### Verwendete Technologien

- [Grav CMS](https://getgrav.org/)
- [Webpack](https://webpack.js.org/)
- [SCSS](https://sass-lang.com/)
- [stimulus.js](https://stimulusjs.org/)
- [ECMAScript 2015 + Babel](http://babeljs.io/)